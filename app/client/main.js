import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.main.onCreated(function(){
  template = this;
  template.subscribe("FormBuilder.dynamicForms");
  template.formUid = new ReactiveVar();
});

Template.main.helpers({
  'forms': function(){
    return FormBuilder.Collections.DynamicForms.find();
  },
  'formUid': function() {
    return Template.instance().formUid;
  },
  'name': function() {
    return Template.instance().formUid.get();
  },
  'data': function() {
    name = Template.instance().formUid.get();
    data = FormBuilder.Collections.DynamicForms.findOne({name: name});
    return JSON.stringify(data,null,2);
  }
});

Template.main.events({
  'click [data-action="form-edit"]': function(event,template) {
    var formId = $(event.target).data('uid');
    template.formUid.set(formId);
  }
});
