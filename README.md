Test application for the autoform-builder package that is a wrapper on the
npm library [formbuilder](https://formbuilder.online/)

The meteor package autoform-builder is a git submodule of this repository.

To test the application simply run this command in the app directory

    METEOR_PACKAGE_DIRS=../packages/  meteor npm install --save babel-runtime
    METEOR_PACKAGE_DIRS=../packages/ meteor npm install --save simpl-schema
    METEOR_PACKAGE_DIRS=../packages/ meteor


Docker
==

You can also run this project in a docker container:

Prerequisites

- working docker installation
- docker-compose

The first time, you should run this commands to initialize all npm modules
used by the application.

    cd app
    meteor npm install
    cd ../
    docker-compose up
